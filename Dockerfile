# centos-nodejs-mini

ARG DOCKER_REGISTRY_URL=registry.gitlab.com/docking/
ARG CUSTOM_VERSION=0.3
ARG CENTOS_VERSION=7
ARG NODEJS_VERSION=12.16.3

ARG NODEJS_URL=https://rpm.nodesource.com/pub_12.x/el/7/x86_64


FROM ${DOCKER_REGISTRY_URL}centos:${CUSTOM_VERSION}-${CENTOS_VERSION} AS centos-nodejs-mini

ARG NODEJS_URL
ARG NODEJS_VERSION

WORKDIR /root

RUN \
	yum install -y \
		${NODEJS_URL}/nodejs-${NODEJS_VERSION}-1nodesource.x86_64.rpm \
		${NODEJS_URL}/nodejs-devel-${NODEJS_VERSION}-1nodesource.x86_64.rpm

